# Vamos Começar

## Para executar o aplicativo
Execute o seguinte comando a partir do terminal do VSCode 
``` shell
gradlew run
```
#### Para atrir o terminal no VSCode
* use a sequência 
```
Ctrl+`
```
### Documentação de Referência
Para futuras referências, siga as seções a seguir: 

* [Official Gradle documentation](https://docs.gradle.org)
* [JavaFX Main Site](https://openjfx.io)
* [JavaFX Documentation Project](https://fxdocs.github.io/docs/html5/)
* [JavaFX Game Engine](https://github.com/AlmasB/FXGL)


### Links Adicionais
Esta referência adicional pode ser de grande ajuda a você:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

