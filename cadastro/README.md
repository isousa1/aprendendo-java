# Vamos Começar

## Para executar o aplicativo execute o seguinte comando a partir do terminal do VSCode
``` shell
gradlew bootRun
```
#### Para atrir o terminal no VSCode
* use a sequência 
```
Ctrl+`
```
### Documentação de Referência
Para futuras referências, siga as seções a seguir: 

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.2/gradle-plugin/reference/html/)
* [Spring Boot Reference](https://docs.spring.io/spring-boot/docs/2.5.2/reference/htmlsingle/#legal)

### Guias
Os guias a seguir ilustram como utilizar alguns recursos na prática:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)


### Links Adicionais
Esta referência adicional pode ser de grande ajuda a você:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

