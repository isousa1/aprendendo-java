package br.senai.sp.informatica.cadastro.component;

public class DataConflictException extends DataException {
    public DataConflictException(String message) {
        super(message);
    }

    public DataConflictException(String message, Throwable t) {
        super(message, t);
    }
}
