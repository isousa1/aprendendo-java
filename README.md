# Aprendendo JAVA - Dia 15/07/2021
## Bem Vindo
Aqui são disponibilizados o material da apresentação Aprendendo Java.
## Como obter
Para obter uma cópia deste conteúdo basta utilizar o comando:

```
git clone git@gitlab.com:JSWilProf/aprendendo-java.git
```

Também é possível fazer o download através do link
[Aprendendo Java](https://gitlab.com/JSWilProf/aprendendo-java/-/archive/master/aprendendo-java-master.zip)

O link da apresentação pode ser encontrada no [YouTube](https://youtu.be/eYtDn_NTTzs)
# Ementa

## Módulo Principal (30 minutos)

- A Plataforma Java
- Uma aplicação Spring BOOT
